import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnTest {
    Luhn luhn = new Luhn();
    @Test
    void checkIsLongEnough(){
        assertTrue(luhn.isLongEnough("898989898989898 9"));
        assertFalse(luhn.isLongEnough("8"));
        assertFalse(luhn.isLongEnough("89898989898989 8"));
        assertFalse(luhn.isLongEnough(""));
    }


    @Test
    void checkFindProvidedCheckDigit(){
        assertEquals(2, luhn.findProvidedCheckDigit("424242424242424 2"));
        assertNotEquals(4, luhn.findProvidedCheckDigit("424242424242424 2"));
        assertNotEquals(8, luhn.findProvidedCheckDigit("843263746374635 4"));
        assertEquals(4, luhn.findProvidedCheckDigit("843263746374635 4"));
    }

    @Test
    void checkConvertInputToInt(){
        assertArrayEquals(new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}, luhn.convertInputToInt("555555555555555 5"));
        assertNotEquals(new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}, luhn.convertInputToInt("555555555555555 5"));
    }

    @Test
    void checkCalculateCheckDigit(){
        assertEquals(2, luhn.calculateCheckDigit("424242424242424 2"));
        assertEquals(3, luhn.calculateCheckDigit("1789372997 3"));
    }

    @Test
    void checkDigitsMatch() {
        assertTrue(luhn.digitsMatch(3, 3));
        assertFalse(luhn.digitsMatch(2, 6));
    }
}