import java.util.Scanner;

public class Luhn {

    //Checks if the provided number matches the length of a credit card number
    public boolean isLongEnough(String number) {
        if(convertInputToInt(number).length == 16){
            return true;
        }
        return false;
    }

    //Returns the provided check digit of the provided checksum
    public int findProvidedCheckDigit(String number) {
        int[] numbers = convertInputToInt(number);
        return numbers[numbers.length-1];
    }

    //Converts the input string to an int array for easy manipulation
    public int[] convertInputToInt(String number) {
        try {
            number = number.replaceAll("\\s", ""); //Remove whitespace
            int[] numbers = new int[number.length()];
            String[] numbersString = number.split("");

            //For every character in the string, convert it to an int and add to a new array
            for(int i = 0; i < number.length(); i++){
                numbers[i] = Integer.parseInt(numbersString[i]);
            }
            return numbers;
        } catch (Exception e){
            e.printStackTrace();
        }
        return new int[]{0};
    }

    public boolean digitsMatch(int provided, int calculated){
        return provided == calculated;
    }

    //Removes the check digit from the provided number and calculates the valid check digit
    public int calculateCheckDigit(String number) {
        int[] numbers = convertInputToInt(number);
        int sum = 0;
        boolean alternate = true;
        for(int i = 0; i < numbers.length-1; i++){ //Do not calculate with last digit
            int digit = numbers[i];
            //Every other digit gets multiplied by 2
            if(alternate){
                digit = numbers[i]*2;
                if(digit > 9){
                    digit = (digit % 10) + 1; //Use sum of digits instead
                }
            }
            sum += digit;
            alternate = !alternate;
        }
        return (sum*9)%10; //The valid check digit for the given number
    }

    //Takes the input and prints calculated results
    public void checkNumber(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("What number do you want to check?");
        try{
            String number = scanner.nextLine();
            int length = number.length();
            String numberWithoutCheckDigit = number.substring(0, number.length()-1);
            String checkDigit = number.substring(number.length()-1, number.length());
            number = numberWithoutCheckDigit + " " + checkDigit;
            System.out.println("Input: " + number);
            System.out.println("Provided: " + findProvidedCheckDigit(number));
            System.out.println("Calculated: " + calculateCheckDigit(number));
            if(digitsMatch(findProvidedCheckDigit(number), calculateCheckDigit(number))){
                System.out.println("Checksum: Valid");
            } else{
                System.out.println("Checksum: Not valid");
            }
            if(isLongEnough(number)){
                System.out.println("Digits: " + length + " (Credit card)");
            } else{
                System.out.println("Digits: " + length);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
